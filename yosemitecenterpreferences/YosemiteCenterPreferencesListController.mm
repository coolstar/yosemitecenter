#import "YosemiteCenterPreferencesListController.h"

#import "Preferences/PSSpecifier.h"

#define PLIST_FILE @"/var/mobile/Library/Preferences/com.cortexdevteam.yosemitecenter.plist"

#ifndef kCFCoreFoundationVersionNumber_iOS_7_0
#define kCFCoreFoundationVersionNumber_iOS_7_0 847.2
#endif

#define iOS(X) (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_##X##_0)

#define iOS7 iOS(7)

#define YEAR [[[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:[NSDate date]] year]

static BOOL dataRefreshNeeded = NO;

@implementation YosemiteCenterPreferencesListController

- (id)specifiers {
	if (_specifiers == nil) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            _specifiers = [self loadSpecifiersFromPlistName:@"YosemiteCenteriPhone" target:self];
        }
        else {
            _specifiers = [self loadSpecifiersFromPlistName:@"YosemiteCenteriPad" target:self];
        }
	}
    
	return _specifiers;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return [NSString stringWithFormat:@"© 2014-%lu Jonas Gessner, CoolStar\nFollow us on Twitter: @JonasGessner, @coolstarorg", (unsigned long)YEAR];
    }
    else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 1) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:PLIST_FILE];
        NSString *detail;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            detail = @"1";
        }
        else {
            detail = @"3";
        }
        if (dict != nil && [dict objectForKey:@"Fingers"] != nil) {
            detail = @"";
            int i = 0;
            for (NSNumber *num in [dict objectForKey:@"Fingers"]) {
                if (i == 0) {
                    detail = [NSString stringWithFormat:@"%i",[num intValue]];
                }
                else {
                    detail = [detail stringByAppendingFormat:@", %i",[num intValue]];
                }
                i++;
            }
        }
        /*  if ([detail isEqualToString:@""])
         detail = @"(Disabled)";*/
        cell.detailTextLabel.text = detail;
    }
    else if (indexPath.row == 1 && indexPath.section == 1) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:PLIST_FILE];
        NSString *detail = @"Right";
        if (dict != nil && [dict objectForKey:@"Anchor"] != nil) {
            detail = @"";
            for (NSNumber *num in [dict objectForKey:@"Anchor"]) {
                if ([num intValue] == 1) {
                    detail = @"Left";
                }
                else if (detail.length > 0) {
                    detail = [detail stringByAppendingString:@" and Right"];
                }
                else {
                    detail = @"Right";
                }
            }
        }
        if ([detail isEqualToString:@""]) {
            detail = @"None";
        }
        cell.detailTextLabel.text = detail;
    }
    else if (indexPath.row > 2) {
        cell.detailTextLabel.text = nil;
    }
}

- (NSObject *)enabled:(id)sender {
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:PLIST_FILE];
    NSNumber *theBool = [dict objectForKey:@"Enabled"];
    if (theBool && dict) {
        return theBool;
    }
    else {
        return [NSNumber numberWithBool:YES];
    }
}

- (void)respring {
    system("killall -9 backboardd");
}

- (void)enable:(NSObject *)sth {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:PLIST_FILE];
    if (dict != nil) {
        [dict setObject:sth forKey:@"Enabled"];
    }
    else {
        dict = [NSMutableDictionary dictionaryWithObject:sth forKey:@"Enabled"];
    }
    [dict writeToFile:PLIST_FILE atomically:YES];
    
    CFNotificationCenterPostNotification (CFNotificationCenterGetDarwinNotifyCenter(), CFSTR("com.cortexdevteam.yosemitecenter.settingsChanged"),  NULL, NULL, true);
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (dataRefreshNeeded) {
        [self.table reloadData];
        dataRefreshNeeded = NO;
    }
}

@end


@interface MCAnchorController : PSListController
@end

@implementation MCAnchorController

- (id)specifiers {
	if(_specifiers == nil) {
        NSMutableArray *array = [NSMutableArray array];
        
        PSSpecifier *specifier = [PSSpecifier preferenceSpecifierNamed:@"Right" target:nil set:NULL get:NULL detail:Nil cell:PSListItemCell edit:Nil];
        
        PSSpecifier *specifier2 = [PSSpecifier preferenceSpecifierNamed:@"Left" target:nil set:NULL get:NULL detail:Nil cell:PSListItemCell edit:Nil];
        
        [array addObject:specifier];
        [array addObject:specifier2];
        
        _specifiers = array.copy;
	}
	return _specifiers;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:PLIST_FILE];
    if (dict != nil && [dict objectForKey:@"Anchor"] != nil) {
        if ([[dict objectForKey:@"Anchor"] indexOfObject:[NSNumber numberWithInt:indexPath.row]] != NSNotFound) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if (indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (dict == nil) {
            dict = [NSMutableDictionary dictionaryWithObject:[NSArray arrayWithObject:[NSNumber numberWithInt:0]] forKey:@"Anchor"];
        }
        else {
            [dict setObject:[NSArray arrayWithObject:[NSNumber numberWithInt:0]] forKey:@"Anchor"];
        }
        [dict writeToFile:PLIST_FILE atomically:YES];
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:PLIST_FILE];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        if (dict == nil || [dict objectForKey:@"Anchor"] == nil) {
            dict = [NSMutableDictionary dictionaryWithObject:[NSArray arrayWithObject:[NSNumber numberWithInt:indexPath.row]] forKey:@"Anchor"];
        }
        else {
            NSMutableArray *array = [dict objectForKey:@"Anchor"];
            [array addObject:[NSNumber numberWithInt:indexPath.row]];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
            [array sortUsingDescriptors:[NSArray arrayWithObject:sort]];
        }
        
        [dict writeToFile:PLIST_FILE atomically:YES];
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        NSMutableArray *array = [dict objectForKey:@"Anchor"];
        if ((dict != nil || [dict objectForKey:@"Anchor"] != nil) && ([array count] > 1)) {
            [array removeObject:[NSNumber numberWithInt:indexPath.row]];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
            [array sortUsingDescriptors:[NSArray arrayWithObject:sort]];
            [dict writeToFile:PLIST_FILE atomically:YES];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    dataRefreshNeeded = YES;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CFNotificationCenterPostNotification (CFNotificationCenterGetDarwinNotifyCenter(), CFSTR("com.cortexdevteam.yosemitecenter.settingsChanged"), NULL, NULL, true);
}

@end



@interface MCFingerController : PSListController
@end

@implementation MCFingerController

- (id)specifiers {
	if(_specifiers == nil) {
        NSMutableArray *array = [NSMutableArray array];
        for (unsigned int i = 0; i < 5; i++) {
            PSSpecifier *specifier = [PSSpecifier preferenceSpecifierNamed:[NSString stringWithFormat:@"%u", i+1] target:nil set:NULL get:NULL detail:Nil cell:PSListItemCell edit:Nil];
            [array addObject:specifier];
        }
        _specifiers = array.copy;
	}
	return _specifiers;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:PLIST_FILE];
    if (dict != nil && [dict objectForKey:@"Fingers"] != nil) {
        if ([[dict objectForKey:@"Fingers"] indexOfObject:[NSNumber numberWithInt:indexPath.row+1]] != NSNotFound) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if (indexPath.row == 2 && !(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (dict == nil) {
            dict = [NSMutableDictionary dictionaryWithObject:[NSArray arrayWithObject:[NSNumber numberWithInt:3]] forKey:@"Fingers"];
        }
        else {
            [dict setObject:[NSArray arrayWithObject:[NSNumber numberWithInt:3]] forKey:@"Fingers"];
        }
        [dict writeToFile:PLIST_FILE atomically:YES];
    }
    else if (indexPath.row == 0 && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (dict == nil) {
            dict = [NSMutableDictionary dictionaryWithObject:[NSArray arrayWithObject:[NSNumber numberWithInt:1]] forKey:@"Fingers"];
        }
        else {
            [dict setObject:[NSArray arrayWithObject:[NSNumber numberWithInt:1]] forKey:@"Fingers"];
        }
        [dict writeToFile:PLIST_FILE atomically:YES];
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:PLIST_FILE];
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        if (dict == nil || [dict objectForKey:@"Fingers"] == nil) {
            dict = [NSMutableDictionary dictionaryWithObject:[NSArray arrayWithObject:[NSNumber numberWithInt:indexPath.row+1]] forKey:@"Fingers"];
        }
        else {
            NSMutableArray *array = [dict objectForKey:@"Fingers"];
            
            [array addObject:[NSNumber numberWithInt:indexPath.row+1]];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
            [array sortUsingDescriptors:[NSArray arrayWithObject:sort]];
        }
        
        [dict writeToFile:PLIST_FILE atomically:YES];
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        NSMutableArray *array = [dict objectForKey:@"Fingers"];
        if ((dict != nil || [dict objectForKey:@"Fingers"] != nil) && ([array count] > 1)) {
            
            [array removeObject:[NSNumber numberWithInt:indexPath.row+1]];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
            [array sortUsingDescriptors:[NSArray arrayWithObject:sort]];
            [dict writeToFile:PLIST_FILE atomically:YES];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    dataRefreshNeeded = YES;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CFNotificationCenterPostNotification (CFNotificationCenterGetDarwinNotifyCenter(), CFSTR("com.cortexdevteam.yosemitecenter.settingsChanged"), NULL, NULL, true);
}

@end

