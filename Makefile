ARCHS = armv7 arm64

TARGET = iphone:clang:latest:7.0

THEOS_BUILD_DIR = Packages

FINALPACKAGE = 1

include theos/makefiles/common.mk

TWEAK_NAME = YosemiteCenter
YosemiteCenter_FILES = Tweak.xm
YosemiteCenter_FRAMEWORKS = Foundation UIKit CoreGraphics
YosemiteCenter_CFLAGS = -fno-objc-arc

include $(THEOS_MAKE_PATH)/tweak.mk
SUBPROJECTS += yosemitecenterpreferences
include $(THEOS_MAKE_PATH)/aggregate.mk

after-install::
	install.exec "killall -9 backboardd"