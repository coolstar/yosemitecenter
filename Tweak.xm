#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface YCGestureRecognizer : NSObject
@property (nonatomic, assign) UIGestureRecognizerState state;
@property (nonatomic, assign) CGPoint centroid;
@property (nonatomic, assign) NSInteger direction;
@end

@interface YCTouchGrabberWindow : UIWindow
@end

@implementation YCGestureRecognizer : NSObject
@end

@interface SBHandMotionExtractor : NSObject
- (void)clear;
@end

@interface SBLockScreenView : UIView
- (UIView *)topGrabberView;
@end

@interface SBLockScreenViewController : NSObject
- (SBLockScreenView *)lockScreenView;
@end

@interface UIStatusBar : UIView {
	UIView *_foregroundView;
}
@end

@interface SBModeViewController : UIViewController {
	UIView *_headerView;
	UIScrollView *_contentView;
}
- (void)removeViewController:(UIViewController *)controller;
- (void)_loadContentView;
@end

@interface SBNotificationCenterViewController : UIViewController {
	UIStatusBar *_statusBar;
	UIViewController* _missedModeViewController;
}
@end

@interface SBNotificationCenterController : NSObject {
	SBModeViewController *_modeController;
}
+(SBNotificationCenterController *)sharedInstance;
- (SBNotificationCenterViewController *)viewController;
-(void)presentAnimated:(BOOL)animated;
-(void)presentAnimated:(BOOL)animated completion:(id)completion;
-(void)dismissAnimated:(BOOL)animated;
-(void)dismissAnimated:(BOOL)animated completion:(id)completion;
@end

@interface SpringBoard : UIApplication
- (UIInterfaceOrientation)activeInterfaceOrientation;
- (void)handleYosemiteCenterPan:(YCGestureRecognizer *)panGestureRecognizer;
- (void)_rotateView:(UIView *)view toOrientation:(UIInterfaceOrientation)orientation;
@end

@interface UIKeyboard : UIView
+ (bool)isOnScreen;
@end

#define PLIST_FILE @"/var/mobile/Library/Preferences/com.cortexdevteam.yosemitecenter.plist"
NSMutableDictionary *prefs = nil;
static BOOL isFirstTouch = YES;
static BOOL isTrackingNotificationCenterSwipe = NO;
static BOOL YCFullyRevealed = NO;
static YCGestureRecognizer *gestureRecognizer = [[YCGestureRecognizer alloc] init];
static UIWindow *touchGrabber = nil;
static BOOL enabled = YES;
static NSArray *anchor = nil;
static NSInteger sensitivity = 1;

NS_INLINE float areaForTouches(NSInteger fingers, NSInteger overrideSensitivity) {
	NSInteger x = fingers;
    CGFloat returnValue = 0.0f;
    NSInteger orig = sensitivity;
    if (overrideSensitivity > 0) {
        sensitivity = overrideSensitivity-1;
    }
    BOOL iPhone = ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad);
    if (iPhone) {
        if (sensitivity == 0) {
            returnValue = (1.25f*x*x+5.0f*x+3.75f);
        }
        else if (sensitivity == 1) {
            returnValue = (1.875f*x*x+10.0f*x+3.125f);
        }
        else {
            returnValue = (8.75f*x*x+5.0f*x+6.25f);
        }
    }
    else {
        if (sensitivity == 0) {
            returnValue = (2.5f*x*x+30.0f*x-12.5f);
        }
        else if (sensitivity == 1) {
            returnValue = (3.125f*x*x+75.0f*x-53.125f);
        }
        else {
            returnValue = (4.375f*x*x+115.0f*x-84.375f);
        }
    }
    
    sensitivity = orig;
    
    // Make it easy to pull closed, I want it sensitive to open but easy to close.
    if (YCFullyRevealed && iPhone) {
        return 20.0f; // This is to fix an annoyance I've had using it for the past few days.
    }
    
    return returnValue;
}

@implementation YCTouchGrabberWindow //This window is shown to block touches when the NC is opened
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
	SBNotificationCenterController *notificationCenterController = [%c(SBNotificationCenterController) sharedInstance];
	UIViewController *notificationCenterViewController = [notificationCenterController viewController];
	UIView *notificationCenterView = notificationCenterViewController.view;
	CGRect notificationCenterFrame = notificationCenterView.frame;

	if (isTrackingNotificationCenterSwipe) //block all touches when swiping
		return YES;
	else if (CGRectContainsPoint(notificationCenterFrame,point)){ //allow touches inside the NC
		return NO;
	}
	return YES; //block all other touches
}
@end

static void updateTouchGrabber(){ //update the orientation of the touch-blocking window
	SpringBoard *sb = (SpringBoard *)[UIApplication sharedApplication];
	[sb _rotateView:touchGrabber toOrientation:[sb activeInterfaceOrientation]];
}

static void updateNCStatusBar(){
	SBNotificationCenterController *notificationCenterController = [%c(SBNotificationCenterController) sharedInstance];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
		UIStatusBar *_foregroundView = [[[notificationCenterController viewController] valueForKey:@"_statusBar"] valueForKey:@"_foregroundView"];
		if ([anchor count] == 1){
			if ([anchor containsObject:@0]){
				for (UIView *x in _foregroundView.subviews){
					if (x.frame.origin.x < 160){
						x.alpha=0;
					}
				}
			} else {
				for (UIView *x in _foregroundView.subviews){
					if (x.frame.origin.x > 160){
						x.alpha=0;
					}
				}
			}
		}
		for (UIView *x in _foregroundView.subviews){
			if ([x isKindOfClass:[objc_getClass("UIStatusBarTimeItemView") class]])
				x.alpha = 0;
		}
	}
}

static void prefsChanged(CFNotificationCenterRef center, void *observer,CFStringRef name, const void *object, CFDictionaryRef userInfo)
{
	[prefs release];
	prefs = nil;
	prefs = [[NSMutableDictionary alloc] initWithContentsOfFile:PLIST_FILE];
	if (![prefs objectForKey:@"Enabled"])
		[prefs setObject:@YES forKey:@"Enabled"];
	enabled = [[prefs objectForKey:@"Enabled"] boolValue];

	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
		if (![prefs objectForKey:@"Fingers"])
			[prefs setObject:@[@3] forKey:@"Fingers"];
	} else {
		if (![prefs objectForKey:@"Fingers"])
			[prefs setObject:@[@1] forKey:@"Fingers"];
	}
	if (![prefs objectForKey:@"Anchor"])
		[prefs setObject:@[@0] forKey:@"Anchor"];
	if (![prefs objectForKey:@"Sens"])
		[prefs setObject:@1 forKey:@"Sens"];
	sensitivity = [[prefs objectForKey:@"Sens"] intValue];
	anchor = [prefs objectForKey:@"Anchor"];
	if (![prefs objectForKey:@"keyboardDisables"])
		[prefs setObject:@NO forKey:@"keyboardDisables"];
}

%ctor {
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, prefsChanged, CFSTR("com.cortexdevteam.yosemitecenter.settingsChanged"), NULL,CFNotificationSuspensionBehaviorDeliverImmediately);
	prefsChanged(nil,nil,nil,nil,nil); //Add preference notification observer and load preferences
}

%hook SBUIController //Block the default gesture when YosemiteCenter's enabled.
- (void)_showNotificationsGestureBeganWithLocation:(CGPoint)location {
	if (!enabled)
		%orig;
}
- (void)_showNotificationsGestureCancelled {
	if (!enabled)
		%orig;
}
- (void)_showNotificationsGestureChangedWithLocation:(CGPoint)location velocity:(CGPoint)velocity {
	if (!enabled)
		%orig;
}
- (void)_showNotificationsGestureEndedWithLocation:(CGPoint)location velocity:(CGPoint)velocity {
	if (!enabled)
		%orig;
}
- (void)_showNotificationsGestureFailed {
	if (!enabled)
		%orig;
}
-(void)_hideNotificationsGestureCancelled {
	if (!enabled)
		%orig;
}
-(void)_hideNotificationsGestureEndedWithCompletionType:(NSInteger)completionType velocity:(CGPoint)velocity {
	if (!enabled)
		%orig;
}
-(void)_hideNotificationsGestureChanged:(CGFloat)changed {
	if (!enabled)
		%orig;
}
-(void)_hideNotificationsGestureBegan:(CGFloat)began {
	if (!enabled)
		%orig;
}
%end

%hook SBLockScreenHintManager //The lock screen in iOS 7.1.x does gestures a bit differently.
- (CGRect)_topGrabberZone {
	if (!enabled)
		return %orig;
	return CGRectZero;
}
%end

%hook SBNotificationCenterController

%new
- (void)dismiss:(id)recognizer { //callback for the touch blocker
	[self dismissAnimated:YES];
}

-(void)presentAnimated:(BOOL)animated completion:(void (^)())completion {
	if (!enabled){
		%orig;
		return;
	}
	CGFloat NCWidth = [UIScreen mainScreen].bounds.size.width;
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
		NCWidth = 320;

	touchGrabber.hidden = YES;
	[touchGrabber release];
	touchGrabber = nil;
	touchGrabber = [[YCTouchGrabberWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	touchGrabber.windowLevel = 99999; //the touch blocker should try to block as many touches as possible
	touchGrabber.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.01]; //for some reason clearColor doesn't work too well
	touchGrabber.hidden = NO;

	UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
	tapGestureRecognizer.numberOfTapsRequired = 1;
	tapGestureRecognizer.numberOfTouchesRequired = 1;
	[touchGrabber addGestureRecognizer:[tapGestureRecognizer autorelease]]; //dismiss when touch blocker tapped

	updateTouchGrabber(); //update the orientation of the new window

	%orig(NO,nil);
	UIView *notificationCenterView = [self viewController].view;
	CGRect notificationCenterFrame = notificationCenterView.frame;
	if ([anchor containsObject:@0])
		notificationCenterFrame.origin.x = notificationCenterFrame.size.width;
	else {
		notificationCenterFrame.origin.x = -NCWidth;
	}
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
		notificationCenterFrame.size.width = NCWidth;
	else
		notificationCenterFrame.size.width = [UIScreen mainScreen].bounds.size.width;
	notificationCenterView.frame = notificationCenterFrame;

	SBModeViewController *modeController = [[self viewController] valueForKey:@"_modeController"];
	
	UIView *modeSelectionView = [modeController valueForKey:@"_headerView"];
	if (kCFCoreFoundationVersionNumber < 1140){
		UIViewController *_missedViewController = [[self viewController] valueForKey:@"_missedModeViewController"];
		[modeController removeViewController:_missedViewController];
	}
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
		for (UISegmentedControl *x in modeSelectionView.subviews){
			if ([x isKindOfClass:[UISegmentedControl class]]){
				x.frame = CGRectMake(10,17,300,29);
			}
		}

		if (![modeController view])
				[modeController loadView];
		UIScrollView *contentView = [modeController valueForKey:@"_contentView"];
		if (!contentView){
			[modeController _loadContentView];
			contentView = [modeController valueForKey:@"_contentView"];
		}
		CGRect contentViewFrame = contentView.frame;
		contentViewFrame.origin.x = -80.f;
		contentViewFrame.size.width = 800.f;
		contentView.frame = contentViewFrame;
	}

	updateNCStatusBar();

	if ([anchor containsObject:@0])
		notificationCenterFrame.origin.x -= NCWidth;
	else
		notificationCenterFrame.origin.x += NCWidth;
	[UIView animateWithDuration:(animated ? 0.25f : 0.0f) animations:^{
		notificationCenterView.frame = notificationCenterFrame;
	} completion:^(BOOL finished){
		updateNCStatusBar();
		YCFullyRevealed = YES;
		if (completion != nil)
			completion();
	}];
}

-(void)dismissAnimated:(BOOL)animated completion:(void (^)())completion {
	if (!enabled){
		%orig;
		return;
	}
	CGFloat NCWidth = [UIScreen mainScreen].bounds.size.width;
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
		NCWidth = 320;

	updateNCStatusBar();
	YCFullyRevealed = NO;
	touchGrabber.hidden = YES;
	[touchGrabber release];
	touchGrabber = nil;

	UIView *notificationCenterView = [self viewController].view;
	CGRect notificationCenterFrame = notificationCenterView.frame;
	if (notificationCenterFrame.origin.x <= 0)
		notificationCenterFrame.origin.x -= NCWidth;
	else
		notificationCenterFrame.origin.x += NCWidth;
	[UIView animateWithDuration:(animated ? 0.25f : 0.0f) animations:^{
		notificationCenterView.frame = notificationCenterFrame;
	} completion:^(BOOL finished){
		%orig(NO,completion);
	}];
}

- (void)presentAnimated:(BOOL)animated {
	if (!enabled){
		%orig;
		return;
	}
	[self presentAnimated:animated completion:nil];
}

- (void)dismissAnimated:(BOOL)animated {
	if (!enabled){
		%orig;
		return;
	}
	[self dismissAnimated:animated completion:nil];
}
%end

%hook SBModeViewController
- (void)_layoutContentIfNecessary {
	%orig;
	if (!enabled)
		return;
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
		UIScrollView *contentView = [self valueForKey:@"_contentView"];
		contentView.translatesAutoresizingMaskIntoConstraints = YES;
		CGRect contentViewFrame = contentView.frame;
		contentViewFrame.origin.x = -80.f;
		contentViewFrame.size.width = 800.f;
		contentView.frame = contentViewFrame;
	}
	if (kCFCoreFoundationVersionNumber < 1140){
		UIView *modeSelectionView = [self valueForKey:@"_headerView"];
		for (UISegmentedControl *x in modeSelectionView.subviews){
			if ([x isKindOfClass:[UISegmentedControl class]]){
				[x setTitle:@"Notifications" forSegmentAtIndex:1];
			}
		}
	}
}
%end

%hook SpringBoard
%new
- (void)handleYosemiteCenterPan:(YCGestureRecognizer *)panGestureRecognizer {
	if (!enabled)
		return;
	SpringBoard *springboard = (SpringBoard *)[UIApplication sharedApplication];
	CGRect screenSize = [[UIScreen mainScreen] bounds];
	if (UIInterfaceOrientationIsLandscape([springboard activeInterfaceOrientation])){
		screenSize = CGRectMake(0,0,screenSize.size.height,screenSize.size.width);
	}

	CGFloat NCWidth = [UIScreen mainScreen].bounds.size.width;
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
		NCWidth = 320;

	SBNotificationCenterController *notificationCenterController = [%c(SBNotificationCenterController) sharedInstance];
	UIViewController *notificationCenterViewController = [notificationCenterController viewController];
	UIView *notificationCenterView = notificationCenterViewController.view;

	if (panGestureRecognizer.state == UIGestureRecognizerStateBegan){
		[[%c(SBNotificationCenterController) sharedInstance] presentAnimated:NO];
		YCFullyRevealed = NO;
		CGRect frame = notificationCenterView.frame;
		frame.origin.x = screenSize.size.width;
		notificationCenterView.frame = frame;

	} else if (panGestureRecognizer.state == UIGestureRecognizerStateCancelled){
		[[%c(SBNotificationCenterController) sharedInstance] dismissAnimated:YES];
	}

	updateNCStatusBar();

	//CGPoint translation = [panGestureRecognizer translation];
    CGRect frame = notificationCenterView.frame;
    if (panGestureRecognizer.direction == 1){
    	frame.origin.x = panGestureRecognizer.centroid.x;
    	if (frame.origin.x < screenSize.size.width-NCWidth)
        	frame.origin.x = screenSize.size.width-NCWidth;
    }
    if (panGestureRecognizer.direction == 2){
    	frame.origin.x = panGestureRecognizer.centroid.x-NCWidth;
    	if (frame.origin.x > 0.f)
        	frame.origin.x = 0.f;
    }
    notificationCenterView.frame = frame;

    if (panGestureRecognizer.state == UIGestureRecognizerStateEnded){
    	if (panGestureRecognizer.direction == 1){
    		if (frame.origin.x < screenSize.size.width-(NCWidth/2.0f)){
    			frame.origin.x = screenSize.size.width-NCWidth;
    			[UIView animateWithDuration:0.25f animations:^{
    				YCFullyRevealed = YES;
    				notificationCenterView.frame = frame;
    			}];
    		} else {
    			[[%c(SBNotificationCenterController) sharedInstance] dismissAnimated:YES];
    		}
    	}
    	if (panGestureRecognizer.direction == 2){
    		if (frame.origin.x > -(NCWidth/2.0f)){
    			frame.origin.x = 0.f;
    			[UIView animateWithDuration:0.25f animations:^{
    				YCFullyRevealed = YES;
    				notificationCenterView.frame = frame;
    			}];
    		} else {
    			[[%c(SBNotificationCenterController) sharedInstance] dismissAnimated:YES];
    		}
    	}
    }
}

%end

%hook SBHandMotionExtractor
-(void)extractHandMotionForActiveTouches:(void*)activeTouches count:(NSUInteger)count centroid:(CGPoint)centroid {
	if (!enabled){
		%orig;
		return;
	}
	SpringBoard *springboard = (SpringBoard *)[UIApplication sharedApplication];
	CGRect screenSize = [[UIScreen mainScreen] bounds];
	if (UIInterfaceOrientationIsLandscape([springboard activeInterfaceOrientation])){
		screenSize = CGRectMake(0,0,screenSize.size.height,screenSize.size.width);
	}

	SBNotificationCenterController *notificationCenterController = [%c(SBNotificationCenterController) sharedInstance];
	UIViewController *notificationCenterViewController = [notificationCenterController viewController];
	UIView *notificationCenterView = notificationCenterViewController.view;
	if ((!isTrackingNotificationCenterSwipe) && (notificationCenterView.frame.origin.x == 0 || notificationCenterView.frame.origin.x == screenSize.size.width - 320)){
		%orig;
		return;
	}
	if ([[prefs objectForKey:@"keyboardDisables"] boolValue] && [UIKeyboard isOnScreen]) {
		%orig;
		return;
	}
	BOOL countMatches = NO;
	for (NSNumber *fingerNum in [prefs objectForKey:@"Fingers"]){
		if (count == [fingerNum intValue]){
			countMatches = YES;
		}
	}
	if (isFirstTouch && countMatches){
		float area = areaForTouches(count,sensitivity);
		if ([anchor containsObject:@0]){
			if (centroid.x > screenSize.size.width-area){
				isTrackingNotificationCenterSwipe = YES;
				gestureRecognizer.direction = 1;
			}
		}
		if ([anchor containsObject:@1]){
			if (centroid.x < area){
				isTrackingNotificationCenterSwipe = YES;
				gestureRecognizer.direction = 2;
			}
		}
	}
	if (isTrackingNotificationCenterSwipe) {
		if (isnan(centroid.x) || isnan(centroid.y)){
			[self clear];
			return;
		}
		if (isFirstTouch)
			gestureRecognizer.state = UIGestureRecognizerStateBegan;
		else
			gestureRecognizer.state = UIGestureRecognizerStateChanged;
		gestureRecognizer.centroid = centroid;
		SpringBoard *springboard = (SpringBoard *)[UIApplication sharedApplication];
		dispatch_sync(dispatch_get_main_queue(),^{
			[springboard handleYosemiteCenterPan:gestureRecognizer];
		});
		isFirstTouch = NO;
	} else {
		%orig;
	}
}
- (void)clear {
	if (!enabled){
		%orig;
		return;
	}
	if (!isTrackingNotificationCenterSwipe)
		%orig;
	if (isTrackingNotificationCenterSwipe){
		gestureRecognizer.state = UIGestureRecognizerStateEnded;
		SpringBoard *springboard = (SpringBoard *)[UIApplication sharedApplication];
		dispatch_sync(dispatch_get_main_queue(),^{
			[springboard handleYosemiteCenterPan:gestureRecognizer];
		});
	}
	gestureRecognizer.state = UIGestureRecognizerStateBegan;
	gestureRecognizer.centroid = CGPointZero;
	isTrackingNotificationCenterSwipe = NO;
	isFirstTouch = YES;
}
%end

%hook SBWallpaperController
-(void)activeInterfaceOrientationDidChangeToOrientation:(NSInteger)activeInterfaceOrientation willAnimateWithDuration:(CGFloat)duration fromOrientation:(NSInteger)orientation {
	%orig;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		updateTouchGrabber();
	});
}
%end

%hook SBLockScreenView
- (void)layoutSubviews {
	%orig;
	if (enabled)
		[[self topGrabberView] setAlpha:0];
}
%end

%hook SBLockScreenViewController
- (void)activate {
	%orig;
	if (enabled)
		[[[self lockScreenView] topGrabberView] setAlpha:0];
}

- (void)loadView {
	%orig;
	if (enabled)
		[[[self lockScreenView] topGrabberView] setAlpha:0];
}

-(void)_handleDisplayTurnedOn {
	%orig;
	if (enabled)
		[[[self lockScreenView] topGrabberView] setAlpha:0];
}

%end